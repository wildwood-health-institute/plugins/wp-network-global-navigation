<?php

/*
Plugin Name: Multisite Global Navigation
Description: Sync all secondary menus on your wordpress network
Version: 1.5
Author: Daniel Atwood
Text Domain: gnav
 */

namespace gnav;

use function WWCore\enqueue_maybe;
use function WWCore\fetch;
use function WWCore\render;

require_once __DIR__ . '/menu.php';

global $wpdb;
define(__NAMESPACE__ . '\Table', $wpdb->base_prefix . 'gnav_menu');

const VER = 1.5;

register_activation_hook(__FILE__, 'gnav\activate');
register_uninstall_hook(__FILE__, 'gnav\uninstall');

add_action('network_admin_menu', 'gnav\network_menu');
add_action('wp_enqueue_scripts', 'gnav\enqueue');
add_action('wp_ajax_gnav_fetch', function () {fetch(__NAMESPACE__);});

add_filter('wp_nav_menu_items', 'gnav\menu\display', 10, 2);

function activate()
{
    global $wpdb;

    $table_name = Table;
    $charset_collate = $wpdb->get_charset_collate();
    $sql =
        "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            name tinytext NOT NULL,
            url varchar(55) DEFAULT '' NOT NULL,
            position mediumint(9) NOT NULL,
            PRIMARY KEY (id)
        ) $charset_collate;";

    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql);
}

function uninstall()
{
    global $wpdb;
    $wpdb->query('DROP TABLE IF EXISTS ' . Table);
}

function enqueue()
{
    enqueue_maybe(__FILE__, 'gnav_mobile', 'css', [], VER);
    enqueue_maybe(__FILE__, 'gnav_mobile', 'js', [], VER, true);
}

function network_menu()
{
    global $wpdb;
    add_menu_page('Global Navigation', 'Global Nav', 'manage_network', 'gnav', 'gnav\network_page', 'dashicons-menu', 6);
}

function network_page()
{
    wp_enqueue_script('jquery-ui-sortable');
    echo render(__FILE__, 'network_page');
}
