<?php

namespace gnav\menu;

use const gnav\Table;

function create(): int
{
    global $wpdb;
    $max_pos = $wpdb->get_row('SELECT MAX(position) as pos FROM ' . Table);
    $wpdb->insert(Table, [
        'name' => 'temp',
        'position' => $max_pos->pos + 1,
    ]);

    return $wpdb->insert_id;
}

function update(int $id, $meta): bool
{
    global $wpdb;
    return (bool) $wpdb->update(Table, $meta, ['id' => $id]);
}

function update_position(array $data)
{
    foreach ($data as $id => $pos) {
        update($id, ['position' => $pos]);
    }
}

function delete(int $id): bool
{
    global $wpdb;
    return (bool) $wpdb->delete(Table, ['id' => $id]);
}

function display($items, $args)
{
    if (!in_array($args->theme_location, ['secondary-menu', 'footer-menu'])) {
        return $items;
    }

    global $wpdb;
    $table = Table;
    $menu_items = $wpdb->get_results(
        "SELECT name, url
        FROM $table
        ORDER BY position ASC"
    );

    $gnav = '';
    $site_url = site_url('', 'https');

    foreach ($menu_items as $item) {
        $item->target = '_self';

        /**
         * If not relative url
         */
        if ($item->url[0] !== '/') {
            /**
             * If external domain
             */
            if ('https://' . $item->url !== $site_url) {
                $item->target = "_blank";
            }

            $item->url = "//$item->url";
        }

        $gnav .= sprintf('<li class="gnav"><a href="%s" target="%s">%s</a></li>', $item->url, $item->target, $item->name);
    }

    return $items . $gnav;
}
