<?php
namespace gnav\template;

use const gnav\Table;

$table = Table;

global $wpdb;
$menu_items = $wpdb->get_results(
    "SELECT *
    FROM $table
    ORDER BY position"
);

$form_action = esc_url(admin_url('admin-post.php'));
?>

<div class="wrap">
    <div class="row title">
        <h1 class="wp-heading-inline">Global Navigation</h1>
        <button id="new_menu_item" class="page-title-action">Add New</button>
    </div>

    <div class="row">
        <div class="left">
            <ul id="sortable">
                <?php foreach ($menu_items as $item): ?>
                <li id="m<?=$item->id?>" class="ui-state-default menu-item-bar gnav_menu">
                    <div class="handle">
                        <span class="dashicons dashicons-move"></span>
                        <input type="text" class="gnav_name" value="<?=$item->name?>" autocomplete="off" required>
                        <input type="text" class="gnav_url" value="<?=$item->url?>" autocomplete="off" placeholder="URL">
                        <button class="gnav_delete">
                            <span class="dashicons dashicons-no"></span>
                        </button>
                    </div>
                </li>
                <?php endforeach;?>
            </ul>
            <div class="right"></div>
        </div>
    </div>
</div>



<script>
    jQuery( function() {
        jQuery('#sortable').sortable();
        jQuery('#sortable').disableSelection();
    } );
</script>
