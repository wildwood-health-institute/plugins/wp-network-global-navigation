class GnavFetch extends EventFetch {
    constructor(element, event, func) {
        super(element, event, {
            action: 'gnav_fetch',
            func: func
        })
    }
}

class NewMenuBtn extends GnavFetch {
    constructor(button) {
        super(button, 'click', 'menu::create')
    }

    success(id) {
        const li = createElement('li', {
            id: `m${id}`,
            className: 'ui-state-default menu-item-bar gnav_menu'
        })

        const div = createElement('div', {
            className: 'handle'
        })

        const move = createElement('span', {
            className: 'dashicons dashicons-move'
        })

        const name = createElement('input', {
            type: 'text',
            className: 'gnav_name',
            value: '',
            autocomplete: 'off',
            required: true
        })

        const url = createElement('input', {
            type: 'text',
            className: 'gnav_url',
            value: '',
            autocomplete: 'off',
            placeholder: 'URL'
        })

        const delbtn = createElement('button', {
            className: 'gnav_delete'
        })

        const delicn = createElement('span', {
            className: 'dashicons dashicons-no'
        })

        div.appendChild(move)
        div.appendChild(name)
        div.appendChild(url)
        delbtn.appendChild(delicn)
        div.appendChild(delbtn)
        li.appendChild(div)

        select('#sortable').appendChild(li)

        name.focus()
        new UpdateMenu(name)
        new UpdateMenu(url)
        new DeleteMenuBtn(delbtn)
    }

    error(error) {
        window.alert(error)
    }
}

class UpdateMenu extends GnavFetch {
    constructor(input) {
        super(input, 'blur', 'menu::update')
        this.data.id = parseInt(input.closest('li').id.slice(1))
        this.parent = input.closest('li')
    }

    change_icon(old, nw) {
        const icon = this.parent.select('button span')
        icon.classList.replace(`dashicons-${old}`, `dashicons-${nw}`)
        return icon
    }

    handler() {
        this.data.meta = {
            name: this.parent.select('input.gnav_name').value,
            url: this.parent.select('input.gnav_url').value
        }

        this.element.disabled = true
        this.change_icon('no', 'update').style.animation = 'spin 1s infinite forwards linear'

        return true
    }

    success() {
        this.reset()
    }

    error(error) {
        this.reset()
        window.alert(error)
    }

    reset() {
        this.change_icon('update', 'no').style.animation = ''
        this.element.disabled = false
    }
}

class DeleteMenuBtn extends GnavFetch {
    constructor(button) {
        super(button, 'click', 'menu::delete')
        this.data.id = parseInt(this.element.closest('li').id.slice(-1))
    }

    success() {
        this.element.closest('li').classList.add('fadeOut')
    }
}

class UpdateOrder extends GnavFetch {
    constructor(li) {
        super(li, 'click', 'menu::update_position')
    }

    input_disabled(bool) {
        this.element.closest('ul').select('input', 0).forEach(input => input.disabled = bool)
    }

    handler(event) {
        if (event.target !== this.element &&
            !event.target.classList.contains('dashicons-move')) {
            return
        }

        let index = 0
        this.data.order = {}
        this.element.closest('ul').select('li').forEach(li => {
            this.data.order[parseInt(li.id.slice(-1))] = ++index
        })

        this.input_disabled(true)

        return true;
    }

    success() {
        this.input_disabled(false)
    }

    error(error) {
        this.input_disabled(false)
        window.alert(error)
    }
}

new NewMenuBtn(select('#new_menu_item'))
select('.gnav_name', 0).forEach(inpt => new UpdateMenu(inpt))
select('.gnav_url', 0).forEach(inpt => new UpdateMenu(inpt))
select('.gnav_delete', 0).forEach(btn => new DeleteMenuBtn(btn))
select('#sortable .handle', 0).forEach(li => new UpdateOrder(li))